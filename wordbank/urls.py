from django.conf.urls import url

from . import views

app_name = 'wordbank'
urlpatterns = [
    url(r'^$', views.Home_page, name='home_page'),
    url(r'^(?P<word_text>[a-z]+)/detail/$', views.DetailView, name='detail'),
    url(r'^AllView/$', views.AllView, name='allview'),
    url(r'^addform/$', views.AddWordForm, name='addword'),
    url(r'^saveword/$', views.SaveWord, name='saveword'),
    url(r'^search/$', views.SearchWord, name='searchword'),
    url(r'^upload/$', views.Upload, name='upload'),
    url(r'^loadcsv/$', views.Loadcsvfile, name='loadcsv'),
    url(r'^backup/$', views.Backup, name='backup'),
    url(r'^backupxml/$', views.BackupXML, name='backupxml'),
    url(r'^uploadxml/$', views.UploadXML, name='uploadxml'),
    url(r'^loadxml/$', views.Loadxmlfile, name='loadxml'),
]
