import csv
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from django.core.files.storage import default_storage
from .models import Word, Detail
import xml.etree.cElementTree as ET
import time
import datetime
import os
from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement

def Home_page(request):
    top_5 = Word.objects.order_by('-pub_date')[:5]
    top_list_search = Word.objects.order_by('-word_count')[:5]
    top_latest_search = Word.objects.order_by('-word_latest')[:5]
    
    return render(request, 'wordbank/home_page.html',{'top_5':top_5, 'top_list_search':top_list_search, 'top_latest_search':top_latest_search})

def AllView(request):
    all_word_list = Word.objects.order_by('word_text')
    context = {'all_word_list':all_word_list}
    return render(request,'wordbank/allview.html',context)

def DetailView(request, word_text):
    word = get_object_or_404(Word, word_text=word_text) 
    context = {'word':word}
    return render(request,'wordbank/detail.html',context)



def AddWordForm(request):
    return render(request,'wordbank/addword.html')

def SaveWord(request):
    error = None
    if request.method == 'POST':
        add_word = request.POST['add_word']
        add_type = request.POST['add_type']
        add_mean = request.POST['add_mean']
        add_sentence = request.POST['add_sentence']
        add_datetime = timezone.now()
        if add_word == "" or add_type == "" or add_mean == "" or add_sentence == "" :
            error = 'add all box please'
            return render(request,'wordbank/addword.html',{'error':error})
        if Word.objects.filter(word_text=add_word):
            same_word = Word.objects.get(word_text=add_word)
            same_word.detail_set.create(type_text = add_type, mean_text =add_mean,sentence_text=add_sentence  )
                 
        
        else:
            new_word = Word(
            word_text = add_word,
            pub_date  = add_datetime
            )
            new_word.save()
            new_word.detail_set.create(type_text = add_type, mean_text =add_mean,sentence_text=add_sentence  )
            
                      
                
    return HttpResponseRedirect(reverse('wordbank:home_page'))

def SearchWord(request):
    top_list_search = Word.objects.order_by('-word_count')[:5]
    top_latest_search = Word.objects.order_by('-word_latest')[:5]
    if request.method == 'POST':
        search = str(request.POST['searchword'])
        if Word.objects.filter(word_text = None):
            return render(request,'wordbank/home_page.html')
        elif Word.objects.filter(word_text=search):
            word = Word.objects.get(word_text=search)
            word_search = get_object_or_404(Word,id = word.id)
            word_search.word_count += 1 
            word_search.word_latest = timezone.now()
            word_search.save()
     
            return render(request,'wordbank/searchword.html',{'word':word})
       
        
    
    return render(request,'wordbank/showsearch.html',{'search':search})
def ShowSearch(request):
    
    print(top_list_search)
    print(top_latest_search)
    return render(request,'wordbank/home_page.html',{'top_list_search':top_list_search, 'top_latest_search':top_latest_search })


    
    
    
def Upload(request):
    return render(request,'wordbank/upload.html')
def Loadcsvfile(request):
    dirPath = 'wordbank/loadcsv/'
    if request.FILES != {}:  # if have file uploaded
        csvFile = request.FILES['csvFile']
        saveFilePath = default_storage.save(dirPath,csvFile)
        os.rename(saveFilePath,'{}word.csv'.format(dirPath))
        all_word = Word.objects.all()
        for word in all_word:
            word.delete()  # delete all before read word in csv
        with open('wordbank/loadcsv/word.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if row['word'] != '':
                    word = Word(word_text=row['word'])
                    word.save()
                else:
                    word.detail_set.create(type_text=row['type'], mean_text=row['meaning'],sentence_text=row['sample_sentence'])
            
            return HttpResponseRedirect(reverse('wordbank:home_page'))
    else:
        return HttpResponseRedirect(reverse('wordbank:upload'))  # not read file if haven't file uploaded

def Backup(request):
    list_word = Word.objects.order_by('word_text')
    with open('wordbank/backup/backup.csv', 'w') as csvfile:
        fieldnames = ['word', 'date', 'type', 'mean',  'example']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for word in list_word:
            for detail in word.detail_set.all():
                writer.writerow({'word':word.word_text, 'date':word.pub_date, 'type': detail.type_text, 'mean': detail.mean_text,
				 'example': detail.sentence_text})
    return HttpResponseRedirect(reverse('wordbank:home_page'))



def BackupXML(request):
    all_word = Word.objects.all()
    root = ET.Element("wordbank")
    for each_word in all_word:
        word = ET.SubElement(root,"Word")
        word.set("word",each_word.word_text)
        for each_detail in each_word.detail_set.all():
            detail = ET.SubElement(word, "detail")
            ET.SubElement(detail, "type").text = each_detail.type_text
            ET.SubElement(detail, "mean").text = each_detail.mean_text
            ET.SubElement(detail, "sentence").text = each_detail.sentence_text

    tree = ET.ElementTree(root)
    tree.write('wordbank/backup/XML_.xml')
    return HttpResponseRedirect(reverse('wordbank:home_page'))

def UploadXML(request):
    return render(request,'wordbank/uploadxml.html')

def Loadxmlfile(request):
    if request.method == 'POST':
        all_word = Word.objects.all()
        document = ElementTree.parse('wordbank/backup/XML_.xml')
        for word in all_word:
            word.delete()
        for word_ in document.findall('Word'):
            if Word.objects.filter(word_text=word_.attrib['word']).count() == 0:
                new = Word(word_text=word_.attrib['word'])
                new.save()
        all_word = Word.objects.all()
        for save_word in document.findall('Word'):
            _word = save_word.attrib['word']
            for word__ in all_word:
                if word__.word_text == _word:
                    word__.detail_set.create(type_text=save_word.find('type').text,
                                             mean_text=save_word.find('mean').text,
                                             sentence_text=save_word.find('sentence').text)
    return HttpResponseRedirect(reverse('wordbank:home_page')) 

       
        
            
          
 


    
