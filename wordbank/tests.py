from django.core.urlresolvers import resolve
from django.test import TestCase
from wordbank.views import Home_page
from django.http import HttpRequest

from django.template.loader import render_to_string 

class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/') 
        self.assertEqual(found.func,wordbank.views.Home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = Home_page(request)
        expected_html = render_to_string('wordbank/home_page.html')
        self.assertEqual(response.content.decode(), expected_html)
