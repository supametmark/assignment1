from django.contrib import admin
from .models import Word,Detail

# Register your models here.

class DetailInline(admin.TabularInline):
    model = Detail
    extra = 2

class WordAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['word_text']}),
        ('Date information', {'fields': ['pub_date']}),
    ]
    inlines = [DetailInline]
    list_display = ('word_text', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['word_text']
admin.site.register(Word, WordAdmin)
