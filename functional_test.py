from selenium import webdriver
import unittest

class NewVisitorTest(unittest.TestCase):
    def setUp(self): 
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)
    def tearDown(self): 
        self.browser.quit()
    def test_can_start_a_list_and_retrieve_it_later(self): 
    # Edith has heard about a cool new online wordbank app. She goes
    # to check out its homepage
        self.browser.get('http://localhost:8000')
    # She notices the page title and header mention  wordbank
        self.assertIn('WordBank', self.browser.title) 
    # she see Wordbank  title on web app
        self.assertIn('WordBank', self.browser.title)
    # she see header 'wordbank' on web app       
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('WordBank', header_text)    
    # she see menu bar on web app
        menu_text = self.browser.find_element_by_tag_name('a').text
        self.assertIn('All Word', menu_text)
    # she see latest 5 word on web app
        latest_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('Latest 5 Word', latest_text)  
        word_text = self.browser.find_element_by_tag_name('p')
        self.assertTrue('me', word_text)
    # she see search box in wordbank app and she  search   
        inputbox = self.browser.find_element_by_tag_name('tr').text
        self.assertTrue( ' search',inputbox)
   
        
        
if __name__ == '__main__': 
    unittest.main(warnings='ignore')
